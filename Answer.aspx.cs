﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

public partial class Answer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Answers", "/Answer.aspx");
        ((ITmgMasterPage)Master).PageTitle = "Answers";
        ((ITmgMasterPage)Master).PageURL = "/Answers.aspx";
        ((ITmgMasterPage)Master).PageType = "article";

        SqlDataReader dr;

        if (Session["questions"] == null || Session["questions"] == String.Empty || Request.QueryString.HasKeys())
        {
            Session["questions"] = Request.QueryString.ToString();
        }

        var questions = HttpUtility.ParseQueryString(Session["questions"].ToString());

        dr = DB.DbFunctions.GetAnswers();

        if (dr != null)
        {
            if (dr.HasRows)
            {
                StringBuilder sb = new StringBuilder();

                int qid = -1;
                bool displayedOne = false;
                while (dr.Read())
                {
                    if (qid != Convert.ToInt32(dr["QuestionID"]))
                    {
                        qid = Convert.ToInt32(dr["QuestionID"]);
                        if (questions.Get("q" + qid) == "1" || !Convert.ToString(dr["Question"]).EndsWith("?"))
                        {
                            if (displayedOne)
                                sb.Append("</ul>");
                            else
                                displayedOne = true;

                            if (Convert.ToString(dr["Question"]).EndsWith("?"))
                            {
                                sb.AppendFormat("\n<h3>{1}) {0} <span style=\"color:#f00;\">YOU ANSWERED YES</span></h3>", dr["Question"], qid);
                            }
                            else
                                sb.AppendFormat("<br />{0}<br /><br />\n", dr["Question"]);

                            sb.Append("\n<ul>");
                        }
                    }
                    if (questions.Get("q" + qid) == "1" || !Convert.ToString(dr["Question"]).EndsWith("?"))
                    { 
                        sb.AppendFormat("\n<li><a target=\"_blank\" href=\"{1}\">{0}", dr["Answer"], dr["Link"]);
                        if (Convert.ToBoolean(dr["IsSponsored"].ToString()))
                        {
                            sb.Append(" - <span style=\"color:#3366cc;\">(sponsored listing)</span>");
                        }
                        sb.Append("</a>\n</li>");
                    }
                }
                sb.Append("\n</ul>");
                litDescription.Text = sb.ToString() + "<br />";
            }
            dr.Close();
            dr.Dispose();
        }

        if (Request.QueryString["lid"] != null && Request.QueryString["lid"] != "")
        {
            dr = DB.DbFunctions.GetLeadData(int.Parse(Request.QueryString["lid"].ToString()));

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        var forwardedFor = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                        string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
                        //DB.DbFunctions.PostToAllInbox("608", dr["SessionID"].ToString(), dr["emailaddress"].ToString(), dr["firstname"].ToString(), IpAddress, dr["lastname"].ToString(), dr["homephoneno"].ToString(), dr["referid"].ToString());
                    }
                }
            }

            dr.Close();
            dr.Dispose();
        }
    }
}

