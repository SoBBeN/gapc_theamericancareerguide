﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SettlementsRebates : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 5;

    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Open Settlements & Rebates", "/Rebates/");

        SqlDataReader dr;

        int order = 0;

        if(Request.QueryString["order"] != null)
        {
            order = int.Parse(Request.QueryString["order"].ToString());
        }
        dr = DB.DbFunctions.GetSettlementsRebatesList(-1, order);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                int start = 0;
                if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                {
                    lnkPrev.HRef = "/Rebates/?s=" + (start - NB_PER_PAGE).ToString();
                    lnkPrev.Visible = true;
                }

                lnkNext.HRef = "/Rebates/?s=" + (start + NB_PER_PAGE).ToString();
                while (start-- > 0 && dr.Read()) ;

                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string id = dr["ID"].ToString();
                    string link = "<a href=\"" + System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "Rebates/" + id + "/" + Functions.StrToURL(dr["Title"].ToString()) + "\">";

                    sb.Append("<div class=\"coupon\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", Functions.ConvertToString(dr["Title"]), link);
                    sb.AppendFormat("<div class=\"img\">{1}<img src=\"{0}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagessettlementsrebatesurl"] + Functions.ConvertToString(dr["Thumbnail"]), link);
                    sb.Append("<div class=\"desc\">");
                    if (dr["DateExpire"] != DBNull.Value)
                    {
                        DateTime settlementDeadline = Convert.ToDateTime(dr["SettlementDeadline"]);
                        sb.Append("<div class=\"expire\">Settlement Deadline: " + settlementDeadline.ToString("MM/dd/yyyy") + "</div>");
                    }
                    sb.AppendFormat("<div class=\"description\">{0}</div>", Functions.Left(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 440));
                    sb.AppendFormat("<div class=\"button\">{0}<img src=\"/images/CAG_Settlements_button.png\" /></a></div>", link);
                    sb.Append("</div>");
                    sb.Append("</div><div style=\"clear:both;\"></div><hr />");

                    if (i % 3 == 0)
                    {
                        if (Request.Browser.IsMobileDevice)
                        {
                            sb.Append("<div class=\"ad\">");
                            sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                            sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                        }
                    }
                }

                if (i < 3)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        sb.Append("<div class=\"ad\">");
                        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    }
                }

                litCoupons.Text = sb.ToString();
                if (i > NB_PER_PAGE)
                    lnkNext.Visible = true;
            }
            dr.Close();
            dr.Dispose();
        }
    }
}