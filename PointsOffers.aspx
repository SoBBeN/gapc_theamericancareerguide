﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PointsOffers.aspx.cs" Inherits="PointsOffers" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({ publisher: "98c30075-93f7-4baa-9669-a5a1792d12a3", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
    <link href="/css/pointsoffers.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                $('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- HMG - Body --><ins class="adsbygoogle" style="display:inline-block;width:98%;height:50px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="9580178383" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <div id="topcoupon">
        <div class="title"><a href="" runat="server" id="lnkCpnTitle" target="_blank"><asp:Literal runat="server" ID="litTitle" /></a></div>
        <div class="date"><asp:Literal runat="server" ID="litDate" /></div>
        <div class="tags">Categories: <asp:Literal runat="server" ID="litTags" /></div>
        <div class="image">
            <a href="" runat="server" id="lnkCpnImg" target="_blank"><img runat="server" id="imgTopCoupon" class="imgTopCoupon" /></a>
        </div>
        <div class="text"><asp:Literal runat="server" ID="litText" /></div>
        <div class="button">
            <a href="" runat="server" id="lnkBtn" target="_blank">Where to buy!</a>
        </div>
        <div id="socialst">
            <span class='st_facebook_hcount item' displayText='Facebook'></span>
            <span class='st_twitter_hcount item' displayText='Tweet'></span>
            <span class='st_pinterest_hcount item' displayText='Pinterest'></span>
            <span class='st_email_hcount item' displayText='Email'></span>
        </div>
    </div>
    <br />
    <div class="ads_padding_middle"></div>
    <br />
    <div id="poPrevNext">
        <div class="prev image"><a href="javascript:void(0);" runat="server" id="imgLnkPrev"><img runat="server" id="imgPrev" /></a></div>
        <div class="prev lnk"><a href="javascript:void(0);" runat="server" id="lnkPrev">< previous</a></div>
        <div class="next image"><a href="javascript:void(0);" runat="server" id="imgLnkNext"><img runat="server" id="imgNext" /></a></div>
        <div class="next lnk"><a href="javascript:void(0);" runat="server" id="lnkNext">next ></a></div>
        <div style="clear:both;"></div>
        <div class="lnk all"><a href="/Products">SEE ALL</a></div>
    </div>
    <div style="clear:both;"></div>
    <br /><br />
    <div id="fbCommentsPlaceholder">
        <fbc:FbComments id="fbComments1" runat="server" />
    </div>
    <br /><br />
</asp:Content>
