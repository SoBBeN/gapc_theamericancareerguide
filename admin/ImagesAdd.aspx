﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ImagesAdd.aspx.cs" Inherits="ImagesAdd" ValidateRequest="false" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/start/jquery-ui.css" />
    <script src="ckeditor446/ckeditor.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDay.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFile" AssociatedControlID="ImageFile" Text="Image File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="ImageFile" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator0" runat="server" ErrorMessage="<img align='absmiddle' src='../images/warn.gif' /> Required"
                        ControlToValidate="ImageFile" SetFocusOnError="true" />
                </td>
                <div style="float:right; overflow:visible; height:0px; position:relative; right:0px; top:-50px;">
                    <img runat="server" id="imgImageFile" style="max-width:340px;" />
                </div>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblType" AssociatedControlID="ddlType" Text="Type:" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlType">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTitle" AssociatedControlID="txtTitle" Text="Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="150" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" runat="server" ErrorMessage="<img align='absmiddle' src='../images/warn.gif' /> Required"
                        ControlToValidate="txtTitle" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblText" AssociatedControlID="txtText" Text="Text:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtText" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtText.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorText" runat="server" ErrorMessage="<img align='absmiddle' src='../images/warn.gif' /> Required"
                        ControlToValidate="txtTitle" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status:" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDay" AssociatedControlID="txtDay" Text="Active Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDay" runat="server" CssClass="text" Columns="10" MaxLength="10" />
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" />
    </div>
</asp:Content>

