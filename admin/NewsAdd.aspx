﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NewsAdd.aspx.cs" Inherits="NewsAdd" ValidateRequest="false" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"> 
 <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/start/jquery-ui.css" />
    <script src="ckeditor446/ckeditor.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDay.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFile" AssociatedControlID="ImageFile" Text="Image File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="ImageFile" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator0" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="ImageFile" SetFocusOnError="true" />
                </td>
                <div style="float:right; overflow:visible; height:0px; position:relative; right:0px; top:-46px;">
                    <img runat="server" id="imgImageFile" style="height:190px;" />
                </div>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblThumbnail" AssociatedControlID="ImageFile" Text="Thumbnail File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fileThumbnail" /> <a runat="server" id="lnkThumbnail">Edit from Image</a>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTitle" AssociatedControlID="txtTitle" Text="Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="text" Columns="100" Width="400px" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtTitle" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblWebsite" AssociatedControlID="txtWebsite" Text="Website Name:" />
                </td>
                <td>
                    <asp:TextBox ID="txtWebsite" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="150" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblLinkURL" AssociatedControlID="txtLinkURL" Text="Website Link:" />
                </td>
                <td>
                    <asp:TextBox ID="txtLinkURL" runat="server" CssClass="text" Columns="150" MaxLength="150" Width="250px" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtLinkURL" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblShortText" AssociatedControlID="txtShortText" Text="Short Text:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtShortText" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtShortText.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorShortText" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtShortText" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblText" AssociatedControlID="txtText" Text="Text:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtText" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtText.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtText" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status:" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" /> (When unchecked, only visible on dev)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDay" AssociatedControlID="txtDay" Text="Active Day:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDay" runat="server" CssClass="text" Columns="10" MaxLength="10" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtDay" SetFocusOnError="true" />
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" /> &nbsp; &nbsp; &nbsp; 
        <a target="_blank" runat="server" id="lnkPreview" visible="false">See Preview</a>
    </div>
</asp:Content>
