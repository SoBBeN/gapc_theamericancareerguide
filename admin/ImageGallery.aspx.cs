﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ImageGallery : System.Web.UI.Page
{
    protected const string ITEMNAME = "Image";

    protected void Page_PreRender(object sender, EventArgs e)
    {
        divMsg.Visible = false;
        if (!IsPostBack)
        {
            FillGV();
        }
    }

    private void FillGV()
    {
        SqlDataReader dt = DB.DbFunctions.GetAllImageGallery();

        gv.DataSource = dt;
        gv.DataBind();
        if (gv.HeaderRow != null)
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
    }

    protected void GvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gv.DataKeys[e.RowIndex]["ID"].ToString());

        DB.DbFunctions.DeleteImageGallery(id);

        divMsg.InnerHtml = "Selected " + ITEMNAME + " deleted successfully.";
        divMsg.Visible = true;

        FillGV();
    }
}