﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewsAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "News";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetNews(id);

        if (dr.HasRows)
        {
            dr.Read();
            txtLinkURL.Text = Functions.ConvertToString(dr["WebSiteLink"]);
            txtWebsite.Text = Functions.ConvertToString(dr["WebSiteName"]);
            txtDay.Text = Functions.ConvertToString(dr["ActiveDate"]);
            txtShortText.Text = Functions.ConvertToString(dr["ShortText"]);
            txtText.Text = Functions.ConvertToString(dr["Text"]);
            txtTitle.Text = Functions.ConvertToString(dr["Title"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            lnkPreview.HRef = System.Configuration.ConfigurationManager.AppSettings["basepreviewurl"] + "news/" + id + "/" + Functions.StrToURL(txtTitle.Text) + "/";
            imgImageFile.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            lnkPreview.Visible = true;
            RequiredFieldValidator0.Enabled = false;
            btnSave.Text = "Update " + ITEMNAME;

            lnkThumbnail.HRef = "MomentsThumbnail.aspx?type=nw&id=" + hidID.Value + "&f=" + HttpUtility.UrlEncode(Functions.ConvertToString(dr["ImageFilename"]));
            if (dr["Thumbnail"] != DBNull.Value)
                lnkThumbnail.HRef += "&t=" + HttpUtility.UrlEncode(Functions.ConvertToString(dr["Thumbnail"]));
            lnkThumbnail.Disabled = false;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;
            string filename = String.Empty;

            if (ImageFile.HasFile)
            {
                filename = Functions.RemoveSpecialChars(ImageFile.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/news", ref filename, ImageFile.PostedFile.InputStream);
            }

            string thumbnail = String.Empty;
            if (fileThumbnail.HasFile)
            {
                thumbnail = Functions.RemoveSpecialChars(fileThumbnail.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/news", ref thumbnail, fileThumbnail.PostedFile.InputStream);
            }


            if (hidID.Value.Length == 0) //INSERT
            {
                if (filename.Length > 0)
                {
                    id = DB.DbFunctions.InsertNews(filename, txtWebsite.Text, txtLinkURL.Text, txtTitle.Text, txtShortText.Text, txtText.Text, chkActive.Checked, txtDay.Text, thumbnail);

                    //POLL INSERTED SUCCESSFULLY
                    if (id > 0)
                    {
                        hidID.Value = id.ToString();
                        divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                        divMsg.Visible = true;
                        ShowExistingValues();
                    }
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateNews(id, filename, txtWebsite.Text, txtLinkURL.Text, txtTitle.Text, txtShortText.Text, txtText.Text, chkActive.Checked, txtDay.Text, thumbnail);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }
        }
    }

}