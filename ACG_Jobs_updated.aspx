﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ACG_Jobs_updated.aspx.cs" Inherits="hosting_staticpages_ACG_Jobs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5WLQ44F');</script>
<!-- End Google Tag Manager -->
    
    <script src="//cdn.zarget.com/94505/142814.js"></script>
    <title>The American Career Guide</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<%--    <link href="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/c4r-css/reset.css" rel="stylesheet" />--%>
    <link href="css/reset.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <%--<link rel="Stylesheet" type="text/css" href="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/c4r-css/ACG_purple.min.css" />--%>
    <link href="css/ACG_purple.min.css" rel="stylesheet" />
<%--<link rel="Stylesheet" type="text/css" href="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/c4r-css/ACG_jobs.css" />--%>
    <link href="css/ACG_jobs.css" rel="stylesheet" />
    <script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/c4r-js/Global.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,800' rel='stylesheet' type='text/css' />
    <script type="text/javascript">
<%--        changeFavicon('<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/c4r-images/favicon/aco.ico');--%>
    </script>
    <script type="text/javascript">
        function togglecat() {
            //$("#header .greentext").toggle();
           // $("#categories").toggle();
            $(".ads_top").toggle();
            $(".categories_border_m").toggle();
        }

        $(document).ready(function () {
            var strCode = "";

            if (!matchMedia('only screen and (min-width: 310px) and (max-width:728px)').matches) {
                $('.ads_top').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- ACG Listing - Top --><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-0634471641041185" data-ad-slot="3988274381" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
            else {
                //Mobile
                $('.ads_top').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- ACG listing - Body 1 (2) --><ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="6941740780"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
                $('.ads_top').css('height', '250px');
                $('.ads_top').css('text-align', 'center');
                $('.header_ad').append('<div style="padding: 5px 0;"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- HMG - Mobile not responsive --><ins class="adsbygoogle" style="display:inline-block;width:98%;height:50px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="8222158780"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script><\/div>');
            }
        });
    </script>
        <%-- FaceBook Script --%>
        <script type="text/javascript">
            if (matchMedia('(max-width:1228px)').matches) {
            }
            else {
                onload = function () {
                    $(".fb-page-categrory").append('<div class="fb-page" data-href="https://www.facebook.com/The-American-Career-Guide-278524819293248" data-width="340" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>');

                }
            }
    </script>
    <%--End FaceBook Script  --%>
        <!-- Facebook Pixel Code -->
    <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return; n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
                n.queue = []; t = b.createElement(e); t.async = !0;
                t.src = v; s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1310977695682140');
            fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1310977695682140&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
             
</head>
<body runat="server" id="bodytag">
    <div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=191569011393168";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
      <div class="header_ad"></div>
        <div class="header">
            <div class="head">YOU QUALIFY FOR A NEW JOB!</div>
            <div class="signup">
                    <div><a class="fb_link" target="_blank" href="http://seq.theamericancareerguide.com/api/user?site_id=9&email={aff_sub5}&aff_name=content&aff_id=6">Sign Up!</a></div>
                    <div class="fb"><a target="_blank" href="https://www.facebook.com/The-American-Career-Guide-278524819293248"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/fb_icon.png" /></a></div>
                </div>
        </div>
    <div class="banner-bg">
        <div class="div-banner">
                <div class="header-container header-container-job">
                 <div class="catbtn">
                    <div class="btn">
                        <a href="javascript:void(0);" onclick="togglecat();"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/mobilebtn_red.png" /></a></div>
                </div>
                <div class="logo">
                    <a href="http://theamericancareerguide-staging.azurewebsites.net/"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/logo2.png" /></a>
                </div>
                <div class="house">
                    <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/ACG_now_hiring_logo2.png" />
               </div>

            </div>
       </div>
   </div>
    <div class="main_container">
        <div class="ad ads_top"></div>
                        <%-- mobile Menu Here!--%>
            <div class="menu_wrapper">
               <div class="categories_border categories_border_m">
                   <div class="header header-menu">
                            <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/explorecategories.png" />

                   </div>
                     <div class="list"><asp:Literal runat="server" ID="litCategories2"></asp:Literal></div>
               </div>
            </div>
        <form id="form1" runat="server">

    <div class="clear container no-color">
        <div class="job-container">
            <div class="job-form">
                <div class="job-form-container">
                    <div class="fields">
                        <div class="keywords">
                            <label>Job Title or Keyword</label>
                            <input runat="server" id="title" class="" type="text" name="q" placeholder="Job Title or Keyword" />
                        </div>
                        <div class="location">
                            <label>City, State or Zip</label>
                            <input runat="server" id="location" class="" type="text" name="l" placeholder="City, State or ZIP" />
                        </div>
                    </div>
                    <div class="button">
                        <asp:ImageButton runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" />
                    </div>
                    <div class="title"><asp:Literal runat="server" ID="litTitle"></asp:Literal></div>
                    <div class="jobs">
                        <asp:Literal runat="server" ID="litJobs"></asp:Literal>
                        <br />
                        <asp:Literal runat="server" ID="litPaging"></asp:Literal>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </form>
                <%--Menu Here!--%>
               <div class="categories_border">
                   <div class="header header-menu">
                            <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/explorecategories.png" />

                   </div>
                     <div class="list"><asp:Literal runat="server" ID="litCategories"></asp:Literal></div>

               </div>

            <div class="ad ad-side">
                <div>
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- ACG listing - Body 1 (2) -->
                    <ins class="adsbygoogle"
                    style="display:inline-block;width:300px;height:250px"
                    data-ad-client="ca-pub-0634471641041185"
                    data-ad-slot="6941740780"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
           </div>
                <div class="fb-page-categrory"></div>
           <div class="ad ad-side">
                <div>
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- ACG listing - Body 2 (2) -->
                    <ins class="adsbygoogle"
                    style="display:inline-block;width:300px;height:250px"
                    data-ad-client="ca-pub-0634471641041185"
                    data-ad-slot="8418473981"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </div>

    <%--Footer--%>
        <div id="bottom">
            <div class="top">
                <div class="title">The American Career Guide is your free source for information and updates about:</div>
                <div class="bullets">
                   Local Jobs <span>•</span> Resume Building <span>•</span> Interview Tips <span>•</span> Headhunters <span>•</span> Linkedin
                </div>
            </div>
            <div class="bottom">
                <div class="disclosure">
                    The American Career Guide is not affiliated with any of the listed products, brands, or organizers, nor do any of them endorse or sponsor The American Career Guide.<br />
                </div>
                <div class="copy">Copyright &copy; <%=DateTime.Now.Year %> C4R Media Corp.<br />
                    The American Career Guide&#8482;
                    <a href="/" target="_blank">Home</a> | <a href="/Terms.aspx" target="_blank">Terms and Conditions</a> | <a href="/Privacy.aspx" target="_blank">Privacy Policy</a> | <a href="/Unsub.aspx" target="_blank">Unsubscribe</a>
                </div>
            </div>
        </div>
    <%--Footer End--%>
    
    <script type="text/javascript">
                        $(function () {
                            dataLayer.push({ event: 'acgAnalytics' });
                        });
</script>
</body>
</html>
