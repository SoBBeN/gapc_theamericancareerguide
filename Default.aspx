﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
            new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5WLQ44F');</script>
<!-- End Google Tag Manager -->
    <link href="/css/news.css" rel="stylesheet" />
    <link href="/css/lifestyle.css" rel="stylesheet" />
    <link href="/css/jobs.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                //$('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- HMG - Body --><ins class="adsbygoogle" style="display:inline-block;width:98%;height:50px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="9580178383" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div id="content">
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionTop"></asp:Literal></div>
        <asp:Literal runat="server" ID="litImage"></asp:Literal>
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionBtm"></asp:Literal></div>
<%--        <h1 class="clear"><a href="/Lifestyle"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_local_job.png" /><asp:Literal runat="server" ID="litLifestylesTitle">Latest In <span>Career Talk</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="lifestyles">
            <asp:Literal runat="server" ID="litArticles"></asp:Literal>
        </div>--%>
        <br class="clear" />
        <h1><a href="/News"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_news.png" /><asp:Literal runat="server" ID="litTitleAstroTalk">Latest <span>In The News</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="news">
            <asp:Literal runat="server" ID="litNews"></asp:Literal>
        </div>

        <br class="clear" />

        <h1><a href="http://www.theamericancareerguide.com/hosting/staticpages/ACG_Jobs_updated.aspx"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_local_job.png" /><asp:Literal runat="server" ID="Literal1">Latest In <span>Local Jobs</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="jobs">
            <div class="job">
                <div class="img">
                    <a href="http://www.theamericancareerguide.com/hosting/staticpages/ACG_Jobs_updated.aspx">
                        <img alt="15 Common Pieces Of Career Advice That Are Actually False" src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/localjobs.png" />
                    </a>
                </div>
                 <div class="description">
                        <h2 class="title"><a href="http://www.theamericancareerguide.com/hosting/staticpages/ACG_Jobs_updated.aspx">Get Hired Now! Find a Local Job in Your Area</a></h2>
                       Check out our directory of personalized job listings with employers who are looking to hire in your area and field. Searching for a new job can be hard work, so we’re here to make the process a little easier. Search now!
                 </div>
            </div>
        </div>
                     

       <%-- <br class="clear" /><br />
        <h1><a href="/Products"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_homeproducts.png" /><asp:Literal runat="server" ID="Literal1">Latest in <span>Home Products</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="news">
            <asp:Literal runat="server" ID="litProducts"></asp:Literal>
        </div>
        <br class="clear" /><br />
        <div class="ads_padding_middle ads_padding_middle_desktop"></div>
        <h1><a href="/Samples"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_freesamples.png" /><asp:Literal runat="server" ID="Literal2">Latest in <span>Samples</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="news">
            <asp:Literal runat="server" ID="litSamples"></asp:Literal>
        </div>
        <br class="clear" />
        <hr />--%>
      <%--  <div class="ads_padding_top ads_padding_middle"></div>--%>
    </div>

    <div class="clear"></div>
    <br />
        
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OutsideForm" Runat="Server">
        <script type="text/javascript">
            $(function () {
                dataLayer.push({ event: 'hmgAnalytics' });
            });
    </script>
</asp:Content>




