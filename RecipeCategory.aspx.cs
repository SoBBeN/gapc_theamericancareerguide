﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RecipeCategory : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Recipe", null);

        int id;
        string categoryType = string.Empty;
        string category = string.Empty;

        if (!int.TryParse(Convert.ToString(Request.QueryString["id"]), out id))
        {
            if (int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
            {
                categoryType = Convert.ToString(Page.RouteData.Values["categoryType"]);
                category = Convert.ToString(Page.RouteData.Values["category"]);
            }
            else
                id = -1;
        }
        else
        {
            categoryType = Request.QueryString["categoryType"];
            category = Request.QueryString["category"];
        }

        category = category.Replace("_", " ");

        SqlDataReader dr;
        StringBuilder sb;

        if (id > 0)
        {
            //((ITmgMasterPage)Master.Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            breadcrumbs1.AddLevel(category, "Recipes/" + id + "/" + Functions.StrToURL(categoryType) + "/" + Functions.StrToURL(category) + "/");
            ((ITmgMasterPage)Master.Master).PageTitle = category;
            string pageurl = "Recipes/" + id + "/" + Functions.StrToURL(categoryType) + "/" + Functions.StrToURL(category) + "/";
            ((ITmgMasterPage)Master.Master).PageURL = pageurl;
            ((ITmgMasterPage)Master.Master).PageType = "article";
            ((ITmgMasterPage)Master.Master).PageDescription = category;
            litTitle.Text = category;

            dr = DB.DbFunctions.GetRecipeByCategory(categoryType, id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int start = 0;
                    if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                    {
                        lnkPrev.HRef = pageurl + "?s=" + (start - NB_PER_PAGE).ToString();
                        lnkPrev.Visible = true;
                    }

                    lnkNext.HRef = pageurl + "?s=" + (start + NB_PER_PAGE).ToString();
                    while (start-- > 0 && dr.Read()) ;


                    sb = new StringBuilder();
                    sb.Append("<hr />");
                    int i = 0;
                    while (dr.Read() && i++ < NB_PER_PAGE)
                    {
                        string link = "<a href=\"/Recipe/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "\">";

                        sb.Append("<div class=\"otherRecipe\"><div class=\"img\">");
                        //litDate.Text = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d, yyyy"));
                        sb.Append(link).AppendFormat("<img src=\"{0}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Functions.ConvertToString(dr["ImageFilename"]));
                        sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", Functions.ConvertToString(dr["Title"]), link);

                        if (dr["CookTime"] != DBNull.Value || dr["PrepTime"] != DBNull.Value || dr["TotalTime"] != DBNull.Value || dr["Portions"] != DBNull.Value)
                        {
                            if (dr["CookTime"] != DBNull.Value)
                                sb.AppendFormat("<span>Cook Time:</span> {0} mins &nbsp; &nbsp; ", dr["CookTime"]);
                            if (dr["PrepTime"] != DBNull.Value)
                                sb.AppendFormat("<span>Prep Time:</span> {0} mins &nbsp; &nbsp; ", dr["PrepTime"]);
                            if (dr["TotalTime"] != DBNull.Value)
                                sb.AppendFormat("<span>Total Time:</span> {0} mins &nbsp; &nbsp; ", dr["TotalTime"]);
                            //if (dr["Portions"] != DBNull.Value)
                            //    sb.AppendFormat("<span>Portions:</span> {0}", dr["Portions"]);
                        }

                        if (dr["Description"] != DBNull.Value)
                            sb.AppendFormat("<div class=\"description\">{0}</div>", dr["Description"]);

                        sb.Append("</div><div style=\"clear:both;\"></div><hr />");

                        litOtherRecipes.Text = sb.ToString();
                    }
                    if (i > NB_PER_PAGE)
                        lnkNext.Visible = true;
                }
                dr.Close();
                dr.Dispose();
            }

        }

        dr = DB.DbFunctions.GetRecipeCategories();

        if (dr != null)
        {
            if (dr.HasRows)
            {
                int groupby = 10;
                sb = new StringBuilder();
                sb.Append("<ul>");
                while (dr.Read())
                {
                    if (groupby != Convert.ToInt32(dr["GroupBy"]))
                    {
                        groupby = Convert.ToInt32(dr["GroupBy"]);
                        sb.Append("<br />");
                    }

                    sb.AppendFormat("<li><a href=\"/Recipes/{0}/{1}/{2}/\">{3}</a></li>\n", dr["ID"], Functions.StrToURL(dr["Category"]), Functions.StrToURL(dr["Description"]), dr["Description"]);

                }
                sb.Append("</ul>");
                litCategories.Text = sb.ToString();
            }
            dr.Close();
            dr.Dispose();
        }

    }
}