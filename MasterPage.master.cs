﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : ITmgMasterPage
{
    protected bool isRegistred;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            UserSession.InsertImpression();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("\n    <title>");
        if (pagetitle != System.Configuration.ConfigurationManager.AppSettings["title"])
            sb.Append(System.Configuration.ConfigurationManager.AppSettings["title"] + " - ");
        sb.Append(pagetitle);
        sb.AppendLine("</title>");
        sb.Append("    <link rel=\"image_src\" href=\"").Append(HttpUtility.UrlPathEncode(pagelogo)).AppendLine("\" />");

        if (showOGtags)
        {
            sb.Append("    <meta property=\"og:title\" content=\"").Append(pagetitle).AppendLine("\" />");
            sb.Append("    <meta property=\"og:url\" content=\"").Append(pageurl).AppendLine("\" />");
            sb.Append("    <meta property=\"og:image\" content=\"").Append(HttpUtility.UrlPathEncode(pagelogo)).AppendLine("\" />\n");
            sb.Append("    <meta property=\"og:type\" content=\"").Append(pagetype).AppendLine("\" />\n");
            if (pagedescription.Length > 0)
            {
                sb.Append("    <meta property=\"og:description\" content=\"").Append(pagedescription).AppendLine("\" />\n");
                sb.Append("    <meta name=\"description\" content=\"").Append(pagedescription).AppendLine("\" />\n");
            }
            foreach (string img in pageimg)
            {
                sb.Append("    <meta property=\"og:image\" content=\"").Append(HttpUtility.UrlPathEncode(img)).AppendLine("\" />\n");
            }
        }
        lithead.Text = sb.ToString();

        SqlDataReader dr = DB.DbFunctions.GetWebSite();

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                litMenu.Text = Convert.ToString(dr["MenuText"]);
                litHeader.Text = Convert.ToString(dr["ContentHeader"]);
                litHeaderMobile.Text = Convert.ToString(dr["ContentHeaderMobile"]);
                if (litHeaderMobile.Text == null || litHeaderMobile.Text == "")
                {
                    litHeaderMobile.Text = Convert.ToString(dr["ContentHeader"]);
                }
            }
            dr.Close();
            dr.Dispose();
        }

        /*This is the side Navbar section information is token from store Procedure named [dbo].[Content_GetAllCategoriesActive] with a param of 18*/
        dr = DB.DbFunctions.GetContentCategoriesActive();

        if (dr != null)
        {
            string mylink = string.Empty;
            if (dr.HasRows)
            {
                sb = new StringBuilder();
                bool first = true;
                while (dr.Read())
                {
                    if (first)
                        first = false;
                    else
                        sb.Append("<hr />");

                    string link = "/Content/" + Functions.StrToURL(Convert.ToString(dr["ID"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/";
                    bool newTab = false;

                    if (int.Parse(dr["ID"].ToString()) == 125)
                    {
                        link = "/";
                    }

                    if (int.Parse(dr["ID"].ToString()) == 124)
                    {
                        link = "/Lifestyle/";
                    }

                    if (int.Parse(dr["ID"].ToString()) == 126)
                    {
                        link = "/News/";
                    }
					if (int.Parse(dr["ID"].ToString()) == 127)
					{
						link = "/Photos/42/Inspirations";
					}
					if (int.Parse(dr["ID"].ToString()) == 128)
                    {
                        link = "http://www.theamericancareerguide.com/hosting/staticpages/ACG_Jobs_updated.aspx?redir=0&utm_source=MetaReturn_[{aff_id}]&utm_medium=[{sequence_id}]&s_id=[{session_id}]&os_id=[{offer_sequence_id}]";
                    }
                    if (int.Parse(dr["ID"].ToString()) == 272)
                    {
                        link = "/HowItWorks.aspx";
                    }


                    sb.AppendFormat("<a target=\"{2}\" href=\"{0}\">{1}</a>", link, Convert.ToString(dr["Category"]), newTab ? "_blank" : "_self");
                }

                litCategories.Text = sb.ToString();
            }
        }

        dr.Close();
        dr.Dispose();

        dr = DB.DbFunctions.GetInternalAdsActive();

        if (dr != null)
        {
            if (dr.HasRows)
            {
                sb = new StringBuilder();
                while (dr.Read())
                {
                    sb.Append("<div data-adid=\"" + Convert.ToString(dr["Id"]) + "\"><a href=\"" + Convert.ToString(dr["Link"]) + "\" target=\"_blank\"><img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagesinternalads"] + Convert.ToString(dr["ImageFilename"]) + "\" /></a></div>");
                }

                if (Functions.isMobile())
                {
                   // litInternalAds.Text = sb.ToString();
                }
                else
                {
                    //litInternalAdsCat.Text = sb.ToString();
                }
            }
        }

        dr.Close();
        dr.Dispose();
    }
}
