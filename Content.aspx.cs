﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Content : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int id;

        if (!int.TryParse(Convert.ToString(Request.QueryString["id"]), out id))
        {
            if (int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
                id = Convert.ToInt32(Page.RouteData.Values["id"]);
            else
                id = 7;
        }

        SqlDataReader dr;

        if (id > 0)
        {
            dr = DB.DbFunctions.GetContent(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]);
                        if (id != 7)
                            breadcrumbs1.AddLevel(Convert.ToString(dr["Category"]), "/content/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/");
                        ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                        ((ITmgMasterPage)Master).PageURL = "content/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/";
                        ((ITmgMasterPage)Master).PageType = "article";
                        ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["DescriptionBtm"]);


                        litTitle.Text = Functions.ConvertToString(dr["Title"]);
                        litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                        litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                        if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                        {
                            if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                                litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                            else
                                litImage.Text = "<a>";

                            litImage.Text += "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]) + "\" id=\"imgMain\" class=\"img\" />";
                            litImage.Text += "</a>";
                        }
                        else
                            litImage.Visible = false;
                    }
                }
                dr.Close();
                dr.Dispose();
            }
        }

    }
}