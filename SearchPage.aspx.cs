﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Configuration;

public partial class SearchPage : System.Web.UI.Page
{
    #region Membres variables
    #endregion

    #region Properties
    protected string SearchQuery
    {
        get
        {
            string searchQuery = Request.QueryString["q"];

            if (String.IsNullOrEmpty(searchQuery))
            {
                return "mom";
            }
            else
            {
                return searchQuery;
            }
                
        }
    }

    #endregion

    #region Events
    #region Page Events
    protected void Page_Init(object sender, EventArgs e)
    {
        btnSearch.Click += new EventHandler(btnSearch_Click);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
        breadcrumbs1.AddLevel("Search", "SearchPage.aspx");

        if (!IsPostBack)
        {
            
            if (SearchQuery != "mom")
            {
                TxtBoxSearch.Text = SearchQuery;
            }
            DisplaySearchResult(Functions.RemoveSpecialChars(Functions.RemoveHtml(SearchQuery)));
            TxtBoxSearch.Focus();
            
           
        }
    }
    #endregion

    #region Control Events
    void btnSearch_Click(object sender, EventArgs e)
    {
        string SearchKeyWord = Functions.RemoveSpecialChars(TxtBoxSearch.Text);
        if (String.IsNullOrEmpty(SearchKeyWord))
        {
            Response.Redirect("/SearchPage.aspx");
        }

        DisplaySearchResult(Functions.RemoveHtml(SearchKeyWord));
        //Response.Redirect("SearchPage.aspx?q=" + HttpUtility.UrlEncode(TxtBoxSearch.Text));
    }
    #endregion
    #endregion

    #region Methods
    private void DisplaySearchResult(string SearchKeyWord)
        {

            SqlDataReader dr;
            StringBuilder Contener = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            StringBuilder ScriptString = new StringBuilder();
            string CurrentPage = "_current";
            string PageStyle = "display:block;";

            int TotalResult = 0;
            int ElementPerPage = 0; 
            int NumberOfPages = 1;
            
               // Per Adam : 
                //use the Appending Script section if you wanna style de Pager (Styling Line)
               // If you wanna modify the item number per page , make sure to change the number "10"  on the "ElementPerPageCondition"

                dr = DB.DbFunctions.GetSearchResult(Functions.RemoveSpace(SearchKeyWord));
                if (dr != null && dr.HasRows)
                {

                    while (dr.Read())
                    {
                        if (ElementPerPage == 0)
                            sb.Append("<div id =\"p" + NumberOfPages + "\" class=\"pageresults" + CurrentPage + "\" style = \"" + PageStyle + "\" >");

                        sb.Append("<div style = \" width: 644px; height: auto; clear: both; border:1px solid #ffffff; margin-top: 15px;\" class =  \" results\" >");
                        sb.Append("<div style = \" margin-left:15px; margin-top: 15px; clear: both;  width: auto; \"><a style = \" font-weight: bold; color: #0eb2c3; font-size: 16px;\" href = \" http://themommyguide.com" + Functions.RemoveHtml(Convert.ToString(dr["Folder"])) + Functions.RemoveHtml(Functions.StrToURL(Convert.ToString(dr["SubFolder"]))) + " \" >" + Functions.RemoveHtml(Convert.ToString(dr["SubFolder"])) + "</a></div>");
   
                        sb.Append("<br /><div style = \" margin-left:15px; float:left; width: 80px; \"><img width = \"80px\" height = \" 70px \" src = \"http://themommyguide.com/" + Functions.RemoveHtml(Functions.StrToImgUrl(Convert.ToString(dr["Image"]), Convert.ToString(dr["Title"]))) + " \"  /></div>");
                        sb.Append("<div style = \" margin-bottom:60px; float: right; height: 70px; line-height: 16px; overflow : hidden; width : 520px; margin-right:15px;\"><p>" + Functions.RemoveHtml(Convert.ToString(dr["Text"])) + "</p></div>");
                        sb.Append("</div>");

                                
                        TotalResult++;
                        ElementPerPage++;

                        if (ElementPerPage == 10) 
                        {
                            sb.Append("</div>");
                            NumberOfPages++;
                            ElementPerPage = 0;
                        }

                        CurrentPage = "";
                        PageStyle = "display:none;"; 

                    }
                    if (ElementPerPage != 10)
                        sb.Append("</div>");

                    Contener.Append("<p style = \" margin-left:15px; line-height: 20px;\">Search Keyword <span style =\" font-weight: bold; \"> " + SearchKeyWord + " </span><br />");
                    Contener.Append("Total : " + TotalResult + " results found.<br />");
                    Contener.Append("<span>Search Result</span><br /></p>");
                    Contener.Append("<div style = \"  width : 260px;  margin-left: 10px; \" class=\"PagerDiv\"></div>");
                    Contener.Append(sb.ToString());
                    Contener.Append("<br />");
                    Contener.Append("<div style = \" float: left;  bottom: 50px; position: relative; margin-left:10px; \" class=\"PagerDiv\"></div>");
                    LblResult.Text = Contener.ToString();

                   // *********** Appending the script ************//

                    ScriptString.Append("<script type=\"text/javascript\" src=\"js/jquery-1.3.2.js\"></script> ");
		            ScriptString.Append("<script src=\"js/jquery.paginate.js\" type=\"text/javascript\"></script> ");
		            ScriptString.Append("<script type=\"text/javascript\"> ");
                    //Styling LIne 
                    ScriptString.Append("$(function () { $(\".PagerDiv\").paginate({count: " + NumberOfPages + ", start: 1, display: 9, border: false, text_color: \"#79B5E3\", background_color: \"none\", text_hover_color: \"#2573AF\", background_hover_color: \"none\", images: false, mouse: \"press\", ");
                    //*************
                    ScriptString.Append("onChange: function (page) { $(\"#p1\").hide(); ");
		            ScriptString.Append("$(\"._current\", \"#pagination\").removeClass(\"_current\").hide(); ");
                    ScriptString.Append("$(\"#p\" + page).addClass(\"_current\").show(); } }); }); ");
		            
		       
                    ScriptString.Append ("</script>");
                    ScriptLabel.Text = ScriptString.ToString();

                    //**********************************************
                }
                else
                {
                    Contener.Append("<p style = \" margin-left:15px; line-height: 15px;\" >Search Keyword <span style =\" font-weight: bold; \"> " + SearchKeyWord + " </span><br />");
                    Contener.Append("<br />No Listing Found</p>");
                    LblResult.Text = Contener.ToString();
                }
                dr.Close();
                dr.Dispose();
 

        }

    #endregion
}