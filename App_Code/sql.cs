using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

/// <summary>
/// SQL client utility.
/// [Pierre Potvin]
/// </summary>
public static class sql
{
    private const short MAX_RETRIES = 3;
    private static int m_iTimeout = 120;

    public static void SendEmail(string _Message)
    {
        ErrorHandling.SendException("C4R Error", new Exception(), _Message);
    }

    public static SqlConnection GetConnection()
    {
        return new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultDBConn"].ConnectionString);
    }

    public static SqlConnection GetNewConnection()
    {
        SqlConnection c = null;

        bool timeout = true;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                c = GetConnection();
                c.Open();
                timeout = false;
            }
            catch (Exception ex)
            {
                if (c != null)
                    c.Dispose();

                if (retry > MAX_RETRIES || !IsTimeout(ex.Message))
                    throw ex;
            }
        }
        return c;
    }

    public static bool IsTimeout(string msg)
    {
        return (msg.Contains("Timeout") || msg.Contains("transport-level error has occurred") || msg.Contains("connection was forcibly closed") || msg.Contains("The specified network name is no longer available") || msg.Contains("deadlock"));
    }

    public static void ExecNonQuery(string psCommandText)
    {
        ExecNonQuery(psCommandText, null);
    }

    public static void ExecNonQuery(string psCommandText, SqlConnection Connection)
    {
        SqlCommand oCmd = null;

        bool timeout = true;
        long iRecAffected = 0;

        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                if (Connection == null || Connection.State != ConnectionState.Open)
                    Connection = GetNewConnection();
                oCmd = new SqlCommand(psCommandText, Connection);
                oCmd.CommandTimeout = m_iTimeout;
                iRecAffected = oCmd.ExecuteNonQuery();
                timeout = false;
            }
            catch (Exception ex)
            {
                if (retry > MAX_RETRIES || !IsTimeout(ex.Message))
                {
                    ErrorHandling.SendException("", ex, "Command:" + psCommandText);
                    return;
                }
            }
            finally
            {
                if (oCmd != null)
                {
                    if (oCmd.Connection != null)
                    {
                        oCmd.Connection.Close();
                        oCmd.Connection.Dispose();
                    }
                    oCmd.Dispose();
                }
            }
        }
    }

    public static DataRow GetDataRow(string SQL)
    {
        return GetDataRow(SQL, null);
    }

    public static DataRow GetDataRow(string psCommandText, SqlConnection Connection)
    {
        DataTable oDt = GetDataTable(psCommandText, Connection);

        if (oDt == null)
        {
            return null;
        }
        else if (oDt.Rows.Count > 0)
        {
            return oDt.Rows[0];
        }
        else
        {
            return null;
        }
    }

    public static DataTable GetDataTable(string SQL)
    {
        return GetDataTable(SQL, null);
    }

    public static DataTable GetDataTable(string psCommandText, SqlConnection Connection)
    {
        DataSet oDs = GetDataSet(psCommandText, Connection);

        if (oDs == null)
        {
            return null;
        }
        else if (oDs.Tables.Count > 0)
        {
            return oDs.Tables[0];
        }
        else
        {
            return null;
        }
    }

    public static DataSet GetDataSet(string SQL)
    {
        return GetDataSet(SQL, null);
    }

    public static DataSet GetDataSet(string psCommandText, SqlConnection Connection)
    {
        SqlCommand sqlCommand = null;
        SqlDataAdapter oDa = null;

        DataSet oDs = null;

        bool timeout = true;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                if (Connection == null || Connection.State != ConnectionState.Open)
                    Connection = GetNewConnection();
                sqlCommand = new SqlCommand(psCommandText, Connection);
                sqlCommand.CommandTimeout = m_iTimeout;
                oDa = new SqlDataAdapter(sqlCommand);
                oDa.SelectCommand.CommandTimeout = m_iTimeout;

                oDs = new DataSet();
                oDa.Fill(oDs);
                timeout = false;
            }
            catch (Exception sqlEx)
            {
                if (retry > MAX_RETRIES || !IsTimeout(sqlEx.Message))
                {
                    ErrorHandling.SendException("", sqlEx, "Command:" + psCommandText);
                    return null;
                }
            }
            finally
            {
                if (sqlCommand != null)
                {
                    if (sqlCommand.Connection != null)
                    {
                        sqlCommand.Connection.Close();
                        sqlCommand.Connection.Dispose();
                    }
                    sqlCommand.Dispose();
                }
                if (oDa != null)
                    oDa.Dispose();
            }
        }
        return oDs;
    }

    public static object GetScalar(string SQL)
    {
        return GetScalar(SQL, null);
    }

    public static object GetScalar(string psCommandText, SqlConnection Connection)
    {
        SqlCommand oCmd = null;

        object oScalar = null;

        bool timeout = true;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                if (Connection == null || Connection.State != ConnectionState.Open)
                    Connection = GetNewConnection();
                oCmd = new SqlCommand(psCommandText, Connection);
                oCmd.CommandTimeout = m_iTimeout;
                oScalar = oCmd.ExecuteScalar();
                timeout = false;
            }
            catch (Exception sqlEx)
            {
                if (retry > MAX_RETRIES || !IsTimeout(sqlEx.Message))
                {
                    ErrorHandling.SendException("", sqlEx, "Command:" + psCommandText);
                    return null;
                }
            }
            finally
            {
                if (oCmd != null)
                {
                    if (oCmd.Connection != null)
                    {
                        oCmd.Connection.Close();
                        oCmd.Connection.Dispose();
                    }
                    oCmd.Dispose();
                }
            }
        }
        return oScalar;
    }

    public static SqlDataReader GetDataReader(string SQL)
    {
        return GetDataReader(SQL, null);
    }

    public static SqlDataReader GetDataReader(string psCommandText, SqlConnection Connection)
    {
        SqlCommand oCmd = null;

        bool timeout = true;
        SqlDataReader sqlReader = null;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                if (Connection == null || Connection.State != ConnectionState.Open)
                    Connection = GetNewConnection();
                oCmd = new SqlCommand(psCommandText, Connection);
                oCmd.CommandTimeout = m_iTimeout;
                sqlReader = oCmd.ExecuteReader(CommandBehavior.CloseConnection);
                timeout = false;
            }
            catch (Exception ex)
            {
                if (oCmd != null)
                {
                    if (oCmd.Connection != null)
                    {
                        oCmd.Connection.Close();
                        oCmd.Connection.Dispose();
                    }
                    oCmd.Dispose();
                }

                if (retry > MAX_RETRIES || !IsTimeout(ex.Message))
                {
                    ErrorHandling.SendException("", ex, "Command:" + psCommandText);
                    return null;
                }
            }
        }

        return sqlReader;
    }

    #region // q
    public static string q(string Value)
    {
        if (Value == null)
            return String.Empty;
        else
            return Value.Replace("'", "''");
    }

    public static string q(char Value)
    {
        if (Value == null)
            return String.Empty;
        else
            return Value.ToString().Replace("'", "''");
    }

    public static string q(int Value)
    {
        return Value == int.MinValue ? "NULL" : Value.ToString();
    }

    public static string q(long Value)
    {
        return Value == long.MinValue ? "NULL" : Value.ToString();
    }
    
    public static string q(float Value)
    {
        return Value == float.MinValue ? "NULL" : Value.ToString();
    }

    public static string q(decimal Value)
    {
        return Value == decimal.MinValue ? "NULL" : Value.ToString();
    }

    public static string q(DateTime Value)
    {
        return Value == DateTime.MinValue ? "NULL" : "'" + Value.ToString("yyyy-MM-dd HH:mm:ss") + "'";
    }

    public static string q(bool Value)
    {
        return Value ? "1" : "0";
    }
    #endregion

}
