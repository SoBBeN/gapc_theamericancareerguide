using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

public class Retries
{

    private int tries = 0;
    private int maxTries = 3;

    private bool successfull = false;
    public int NbTriesMade
    {
        get { return tries + 1; }
    }

    public Retries(int nbRetries) : this(nbRetries, true)
    {
    }

    public Retries(int nbRetries, bool enabledRetries)
    {
        if (!enabledRetries)
        {
            maxTries = 1;
        }
        else
        {
            maxTries = nbRetries;
        }
    }

    public void HandleException(ref Exception ex, string errorTitle, string errorInfos, bool sendConnectionError)
    {
        successfull = false;
        tries += 1;
        if ((tries >= maxTries))
        {
            if ((sendConnectionError == false) & (ex.Message.IndexOf("Unable to connect") >= 0 | ex.Message.IndexOf("connection was closed") >= 0 | ex.Message.IndexOf("(503) Server Unavailable") >= 0 | ex.Message.IndexOf("timed out") >= 0))
            {
                return;
            }
            ErrorHandling.SendException(errorTitle, ex, errorInfos);
        }
    }

    public bool NeedsRetry()
    {
        if (successfull | tries >= maxTries)
        {
            return false;
        }
        else
        {
            successfull = true;
            return true;
        }
    }

    public bool WillNeedsRetry()
    {
        if (successfull | tries >= maxTries)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool BeenSuccessfull()
    {
        return successfull;
    }

}