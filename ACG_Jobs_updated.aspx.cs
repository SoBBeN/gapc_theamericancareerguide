﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;
using NSS.ROS;
using Newtonsoft.Json;

using System.Data.SqlClient;
using DB;

public partial class hosting_staticpages_ACG_Jobs : System.Web.UI.Page
{
    const int NB_PER_PAGE = 6;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && Request.QueryString["page"] == null)
        {
            FlowDirector.AssertSessionIntegrity(18, false, !String.IsNullOrEmpty(Request.QueryString["clear"]) && Request.QueryString["clear"].ToString() == "1", Page.IsPostBack);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();
        SqlDataReader dr = DbFunctions.GetWebSite();
        /*This is the side Navbar section information is token from store Procedure named [dbo].[Content_GetAllCategoriesActive] with a param of 18*/
        dr = DbFunctions.GetContentCategoriesActive();

        if (dr != null)
        {
            string mylink = string.Empty;
            if (dr.HasRows)
            {
                 sb = new StringBuilder();
                bool first = true;
                while (dr.Read())
                {
                    if (first)
                        first = false;
                    else
                        sb.Append("<hr />");

                    string link = "/Content/" + Functions.StrToURL(Convert.ToString(dr["ID"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/";
                    bool newTab = false;

                    if (int.Parse(dr["ID"].ToString()) == 125)
                    {
                        link = "http://content.theamericancareerguide.com/";
                    }

                    if (int.Parse(dr["ID"].ToString()) == 124)
                    {
                        link = "http://content.theamericancareerguide.com/Lifestyle/";
                    }

                    if (int.Parse(dr["ID"].ToString()) == 126)
                    {
                        link = "http://content.theamericancareerguide.com/News/";
                    }
                    if (int.Parse(dr["ID"].ToString()) == 128)
                    {
                        link = "http://www.theamericancareerguide.com/hosting/staticpages/ACG_Jobs_updated.aspx?title=&location=Fort+Lee%2c+New+Jersey&page=5";
                    }

                    if (int.Parse(dr["ID"].ToString()) == 127)
                    {
                        link = "http://content.theamericancareerguide.com/Photos/35/Inspirations";
                    }


                    sb.AppendFormat("<a target=\"{2}\" href=\"{0}\">{1}</a>", link, Convert.ToString(dr["Category"]), newTab ? "_blank" : "_self");
                }

                litCategories.Text = sb.ToString();
                litCategories2.Text = sb.ToString();
    }
        }

        if (!Page.IsPostBack && Request.QueryString["page"] == null)
        {
            Hashtable session = Sessions.Visitor;
            var intSessionID = (int)Sessions.Visitor["SessionID"];

            ros.Flow_InsertOfferImpression(intSessionID, 304, 304, ImpressionProvider.LandingPage);
        }

        btnSubmit.ImageUrl = "/images/acg_job_button_m.png";

        string IpAddress = Request.UserHostAddress;
        string _q, _l;
        int start;
        int page;

        if (Page.IsPostBack)
        {
            _q = title.Value;
            _l = location.Value;
            start = 0;
            page = 1;
        }
        else
        {
            if (!String.IsNullOrEmpty(Request.QueryString["title"]))
            {
                _q = Request.QueryString["title"];
                title.Value = _q;
            }
            else
                _q = String.Empty;

            if (!String.IsNullOrEmpty(Request.QueryString["location"]))
            {
                _l = Request.QueryString["location"];
                location.Value = _l;
            }
            else
            {
                Hashtable lead = FlowDirector.CurrentLead;
                if (lead != null && lead.ContainsKey("postalcode"))
                {
                    _l = Convert.ToString(lead["postalcode"]);
                    location.Value = _l;
                }
                else
                {
                    _l = String.Empty;
                }
            }

            if (int.TryParse(Request.QueryString["page"], out page))
                start = (page - 1) * NB_PER_PAGE;
            else
            {
                start = 0;
                page = 1;
            }
        }

        if (_l.Length == 0)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = Functions.GetLocationInfoByIPAddress(IpAddress);

            if (xmlDoc.SelectSingleNode("//Response/City") != null && xmlDoc.SelectSingleNode("//Response/RegionName") != null)
            {
                if (xmlDoc.SelectSingleNode("//Response/City").InnerText == "" || xmlDoc.SelectSingleNode("//Response/RegionName").InnerText == "")
                {
                    _l = xmlDoc.SelectSingleNode("//Response/City").InnerText;

                    if (_l.Length == 0)
                    {
                        _l = xmlDoc.SelectSingleNode("//Response/RegionName").InnerText;
                    }
                }
                else
                {
                    _l = xmlDoc.SelectSingleNode("//Response/City").InnerText + ", " + xmlDoc.SelectSingleNode("//Response/RegionName").InnerText;
                }
            }
        }

        //if (_l.Length == 0)
        //{
        //    return;
        //}

        string api_key = "sn8v7e8t7527g9i3n5yiaye4hdij4pnt";
        int site = 1;
        if (!string.IsNullOrEmpty(Request.QueryString["site"]))
        {
            site = int.Parse(Request.QueryString["site"].ToString());
        }

        switch (site)
        {
            case 1:
                api_key = "sn8v7e8t7527g9i3n5yiaye4hdij4pnt";
                break;
            case 2:
                api_key = "kq9wvxctir82yy6ai64q9r6m2ahczid8";
                break;
            case 3:
                api_key = "nksvi9qhc98m8b9yv9v7x7mrt9zygj2g";
                break;
            case 4:
                api_key = "nvayd633jgi3nueccjzwmr68ngrtwyh9";
                break;
            case 5:
                api_key = "y8kcj7jsh4xwspg4dsxbfsmyr5yxdf53";
                break;
            case 6:
                api_key = "sjgvaqfhm42na82s4cw66g73rffrsxee";
                break;
        }

         sb = new StringBuilder();
        sb.Append("https://api.ziprecruiter.com/jobs/v1?api_key=" + api_key);
        sb.Append("&search=").Append(Server.UrlEncode(_q));
        sb.Append("&location=").Append(Server.UrlEncode(_l));
        sb.Append("&radius_miles=25&days_ago=");
        sb.Append("&jobs_per_page=").Append(NB_PER_PAGE); 
        sb.Append("&page=").Append(page); 

        String url = sb.ToString();
        String response;

        try
        {
            response = Functions.DoGet(ref url);

            if(response != string.Empty)
            {
                dynamic dyn = JsonConvert.DeserializeObject(response);
                int totalresults = Convert.ToInt32(dyn.total_jobs.ToString());

                if (bool.Parse(dyn.success.ToString()))
                {
                    sb = new StringBuilder();
                    int i = 0;
                    foreach (var obj in dyn.jobs)
                    {
                        sb.Append("<div class=\"job\">");
                        sb.AppendFormat("<div class=\"job-title\"><a target=\"_blank\" href=\"{0}\">{1}</a></div>", obj.url.ToString(), obj.name.ToString());
                        sb.AppendFormat("<div class=\"job-location\">{0}</div>", obj.location.ToString());
                        sb.AppendFormat("<div class=\"job-desc\">{0}</div>", obj.snippet.ToString());
                        sb.Append("</div>");
                        
                        i++;
                    }

                    litJobs.Text = sb.ToString();

                    int itemsperpage = 6;
                    int pageNumber = (start) / itemsperpage + 1;

                    DoPaging(start, pageNumber, totalresults, itemsperpage, "title=" + Server.UrlEncode(_q) + "&location=" + Server.UrlEncode(_l) + "&");
                }

                litTitle.Text = "Jobs in " + _l;
            }
        }
        catch (Exception ex)
        {
            ErrorHandling.SendException("DoGet ACO Juju", ex, url);
            return;
        }
    }

    private void DoPaging(int intStart, int currentPage, int recordCount, int pageSize, string baseVars)
    {
        int intPageCount = 0;

        intPageCount = (int)Math.Ceiling((double)recordCount / (double)pageSize);

        int intEnd = intStart + pageSize - 1;
        if (intEnd > recordCount)
        {
            intEnd = recordCount;
        }

        StringBuilder html = new StringBuilder();

        if (recordCount < 10)
        {
            html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + intEnd + " of " + recordCount + "</div>");
        }
        else if (currentPage == intPageCount)
        {
            if (Convert.ToBoolean(recordCount % 10))
            {
                html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + intEnd + " of " + recordCount + "</div>");
            }
            else
            {
                html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + (intEnd + 1) + " of " + recordCount + "</div>");
            }

        }
        else
        {
            html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + (intEnd + 1) + " of " + recordCount + "</div>");
        }

        html.AppendLine("<div class=\"paging\">");
        html.AppendLine("<div class=\"paging_left\">");

        if ((currentPage != 1))
        {
            //html.AppendLine("<a class=\"pager-a\" href=\"ACG_Indeed.aspx?" + baseVars + "page=1\">First</a>&nbsp;");
            html.AppendLine("<a class=\"pager-a\" href=\"ACG_Jobs.aspx?" + baseVars + "page=" + (currentPage - 1) + "\">Previous</a>&nbsp;&nbsp;");
        }

        html.AppendLine("</div><div style=\"display: inline-block;\"><ul class=\"ul-pager\">");

        int pageStart, pageEnd;
        if (currentPage - 4 < 1)
            pageStart = 1;
        else
            pageStart = currentPage - 4;
        if (currentPage + 4 > intPageCount)
            pageEnd = intPageCount;
        else
            pageEnd = currentPage + 4;

        for (int i = pageStart; i <= pageEnd; i++)
        {
            if (i == currentPage)
            {
                html.AppendLine("<li class=\"selected\">" + i.ToString() + "</li>");
            }
            else
            {
                html.AppendLine("<li><a href=\"ACG_Jobs_updated.aspx?" + baseVars + "page=" + i + "\">" + i + "</a></li>");
            }
        }
        html.AppendLine("</ul></div>");
        html.AppendLine("<div class=\"paging_right\">");

        if ((currentPage < intPageCount))
        {
            html.AppendLine("&nbsp;&nbsp;<a class=\"pager-a\" href=\"ACG_Jobs_updated.aspx?" + baseVars + "page=" + (currentPage + 1) + "\">Next</a>");
        }
        html.AppendLine("</div>");
        html.AppendLine("</div>");
        litPaging.Text = html.ToString();

    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
    }
}